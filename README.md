# sketchpad

A webpage for making simple drawings. Multiple color and resolution options are
provided.

The motivation behind making this webpage was to practice using JavaScript to
change the DOM.

## Usage

[Click here](https://emerac.gitlab.io/sketchpad) to visit the live page!

Alternatively, you can clone this repository and then open the `index.html`
file in your browser.

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.
