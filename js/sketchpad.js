/* Use the mouse to draw on a sketchpad (a grid of div elements).
 *
 * Copyright (C) 2021 emerac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
// Set up the default grid.
createGrid(32);

const clearButton = document.querySelector('.clear-button');
clearButton.addEventListener('click', clearGrid);
const changeButton = document.querySelector('.change-button');
changeButton.addEventListener('click', changeGridSize);
const selectBox = document.querySelector('.pen-select');
selectBox.selectedIndex = "0";


function changeCellColor(e) {
    const selectBox = document.querySelector('.pen-select');
    const penChoice = selectBox.value;
    switch (penChoice) {
        case 'black':
            e.target.style.background = 'rgb(0, 0, 0)';
            break;
        case 'rainbow':
            const red = Math.floor(Math.random() * 256);
            const green = Math.floor(Math.random() * 256);
            const blue = Math.floor(Math.random() * 256);
            e.target.style.background = `rgb(${red}, ${green}, ${blue})`;
            break;
        case 'shader':
            const backgroundColor = e.target.style.backgroundColor;
            const re = /\d{1,3}/;
            let lightness = Number(backgroundColor.match(re)[0]);
            /* Converting to HSL would be more accurate, but that level of
             * accuracy is not necessary here. Instead, break RGB's 255 into
             * 10 chunks and remove a chunk after each mouseenter event.
             * Each chunk is 25.5, but RGB values only accept integers, which
             * means the last chunk will have 5 more than a normal chunk.
             */
            if (lightness === 30) {
                lightness -= 30;
            } else if (lightness > 30) {
                lightness -= Math.floor(255/10);
            } else {
                lightness = 0;
            }
            e.target.style.background = (
                `rgb(${lightness}, ${lightness}, ${lightness})`
            );
            break;
        default:
            throw new Error('Unreachable');
    }
}


function changeGridSize(e) {
    newGridSize = getNewGridSize();
    if (newGridSize === null) {
        return;
    }
    clearGrid();
    const grid = document.querySelector('.grid');
    grid.style.gridTemplate = (
        `repeat(${newGridSize}, 1fr) / repeat(${newGridSize}, 1fr)`
    );
    createGrid(newGridSize);
}


function clearGrid(e) {
    const cells = document.querySelectorAll('.cell');
    cells.forEach(cell => cell.style.background = '#ffffff');
}


function createGrid(sideLength) {
    const grid = document.querySelector('.grid');
    for (let i = 1; i <= sideLength; i++) {
        for (let j = 1; j <= sideLength; j++) {
            const div = document.createElement('div');
            div.classList.add('cell');
            div.style.gridRow = '${i}';
            div.style.gridColumn = '${j}';
            grid.appendChild(div);
            div.style.background = '#ffffff';
        }
    }
    const cells = document.querySelectorAll('.cell');
    cells.forEach(cell => cell.addEventListener('mouseenter', changeCellColor));
}


function getNewGridSize() {
    let size = '';
    let regularPrompt = (
        'Enter the number of squares per side for the new grid (100 max) ' +
        'or press ESC to cancel:'
    );
    size = prompt(regularPrompt);
    while (size !== null && (Number(size) < 1 || Number(size) > 100)) {
        size = prompt('Please enter a valid number.\n' + regularPrompt);
    }
    return size;
}
